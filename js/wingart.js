
var tabsBox  = $(".tabs"),
	popup 	 = $("[data-popup]"),
	zoom 	 = $(".cloud-zoom"),
	fancy 	 = $("[data-fancybox]"),
	carousel = $('.carousel');

if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}

if (popup.length){
	include("js/jquery.arcticmodal.js");
}

if (carousel.length){
	include("js/owl.carousel.js");
}

if (zoom.length){
	include("js/cloud-zoom.1.0.2.min.js");
}

if (fancy.length){
	include("js/jquery.fancybox.min.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){
	
	// responsive navigation toggle button
	// $("#resp-toggle-btn").on('click', function(){
	//   	$("body").toggleClass('navTrue');
	// })
	$(".resp_menu, .resp_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');
    })

	// init of tabs
	if (tabsBox.length){
		tabsBox.each(function(){
			var currentTabs = $(this);

			currentTabs.easyResponsiveTabs();
		})
	}

	// init of carousel
  	if (carousel.length){
	  	carousel.each(function(){
	  		var currentCarousel = $(this),
	  			items 			= currentCarousel.data('items'),
	  			singleItem      = false,
	  			itemsDesc 		= currentCarousel.data('items-desc'),
	  			itemsDescSm		= currentCarousel.data('items-desc-sm'),
	  			itemsTab 		= currentCarousel.data('items-tab');

	  		if (items < 1){
	  			return false;
	  		}

	  		if (items == 1){
	  			singleItem = 1;
	  			itemsDesc = 1;
		  			itemsDescSm = 1;
	  			itemsTab = 1;
	  		}

	  		currentCarousel.owlCarousel({
	  			items : items,
	  			singleItem : singleItem,
	  			itemsDesktop : [1199, itemsDesc],
		        itemsDesktopSmall : [979, itemsDescSm],
		        itemsTablet : [768, itemsTab],
		        itemsTabletSmall : false,
		        itemsMobile : [479, 1],
		        navigation : true,
		        navigationText : [" ", " "],
		        autoplaySpeed: true,
		        autoplayTimeout: 14000,
		        autoHeight : true
	  		})
	  	})
	}
	

	/*------------   Input number   ------------*/

		$('.up').on('click', function(e){
			
			var inputVal = $(this).closest('.input_num_wrap').find('.input_num'),	
				currentVal = parseInt(inputVal.val());

			inputVal.val(currentVal += 1);

			e.preventDefault();
		});

		$('.down').on('click', function(e){
			var inputVal = $(this).closest('.input_num_wrap').find('.input_num'),
				currentVal = parseInt(inputVal.val());

			if(currentVal >= 1){
				inputVal.val(currentVal -= 1);
			}

			e.preventDefault();
		});

	/*------------   Input number END   ------------*/



	/*------------   DropDown List   ------------*/
		
		if($('.catalog__nav--link').length){
			$('.catalog__nav--link').on('click', function(){
				$(this)
				.toggleClass('active')
				.next('.catalog__nav--sub')
				.slideToggle()
				.parents(".catalog__nav--item")
				.siblings(".catalog__nav--item")
				.find(".catalog__nav--link")
				.removeClass("active")
				.next(".catalog__nav--sub")
				.slideUp();
			});  
		}
		$('.catalog__nav--link').on('click', function(e) {
		    e.preventDefault();
		});

	/*------------   DropDown List END   ------------*/



	/*------------   POPUP   ------------*/

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal({
			    		beforeOpen: function(){
			    	}
			    });
			});
		};

	/*------------   POPUP  END  ------------*/


	/*------------   FANCYBOX   ------------*/

		if(fancy.length){
			fancy.fancybox({
				
			});
		};

	/*------------   FANCYBOX  END  ------------*/

	/*------------   ZOOM   ------------*/

		// if(zoom.length){
		// 	fancy.fancybox({
				
		// 	});
		// };

	/*------------   ZOOM  END  ------------*/




})